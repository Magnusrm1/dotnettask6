﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask6
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Adress { get; set; }
        public string ZipCode { get; set; }

        public Person() {}

        public Person(string firstName, string lastName) 
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PhoneNumber = "unknown";
            this.Adress = "unknown";
            this.ZipCode = "unknown";
        }

        public Person(string firstName, string lastName, string phoneNumber, string adress, string zipCode)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PhoneNumber = phoneNumber;
            this.Adress = adress;
            this.ZipCode = zipCode;
        }

        public void PrintPersonInfo()
        {
            // Display stored data of a person.
            Console.WriteLine($"{FirstName} {LastName}: \n" +
                              $"Phone: {PhoneNumber} \n" +
                              $"Adress: {Adress}, {ZipCode} \n");
        }

        public string GetFullName()
        {
            return FirstName + " " + LastName;
        }
    }
}
