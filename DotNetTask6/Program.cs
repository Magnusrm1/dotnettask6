﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DotNetTask6
{
    class Program
    {
        public static List<Person> people = new List<Person>();

        // Lists used to temporarily store search hits.
        public static List<Person> partialMatch = new List<Person>();
        public static List<Person> fullMatch = new List<Person>();

        
        static void Main(string[] args)
        {
            AddPeople();

            // Prompt for new searches untill program is closed.
            while(true)
            {
                NewSearch();
            }
        }

        #region Yellow Pages methods

        public static void AddPeople()
        {
            // Fill data.
            people.Add(new Person("Per", "Pettersen", "43782390", "Gata 4", "4378"));
            people.Add(new Person("Petter", "Skjerven", "48291020", "Veien 2", "3892"));
            people.Add(new Person("Kari", "Nordmann"));
            people.Add(new Person("Ola", "Nordmann"));
            people.Add(new Person("Per", "Knutsen", "38923278", "Plassen 12", "9037"));
        }

        public static void NewSearch()
        {
            // Search hits are cleared each iteration.
            partialMatch.Clear();
            fullMatch.Clear();

            Console.WriteLine("Search a name!");
            String input = Console.ReadLine();

            Search(input, partialMatch, fullMatch, people);

            PrintResults(partialMatch, fullMatch);
        }

        public static void Search(string input, List<Person> partialMatch, List<Person> fullMatch, List<Person> names)
        {
            // Find and store matching names.
            foreach (Person item in names)
            {
                string name = item.GetFullName();
                // If the name matches completely we store the name in fullMatch and
                // dont need it in partial matches.
                if (name == input)
                {
                    fullMatch.Add(item);
                }
                else if (name.Contains(input))
                {
                    partialMatch.Add(item);
                }
            }
        }

        public static void PrintResults(List<Person> partialMatch, List<Person> fullMatch)
        {
            // Display both partially and fully matching names.
            Console.WriteLine("Partial matches:");
            foreach (Person item in partialMatch)
            {
                item.PrintPersonInfo();
            }
            Console.WriteLine("\nFull matches:");
            foreach (Person item in fullMatch)
            {
                item.PrintPersonInfo();
            }
        }
        #endregion
    }
}
